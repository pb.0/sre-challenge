FROM python:3.7-alpine
ADD app /code
WORKDIR /code
RUN pip install -r requirements.txt
ENV FLASK_APP=api.py
ENV REDIS_HOST=redis
ENV REDIS_PORT=6379
CMD ["flask", "run", "--host=0.0.0.0"]

	
