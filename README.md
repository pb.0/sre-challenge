# Solution for SRE Challenge

## Running it

This requires a system with Docker installed.
My host system uses

- Ubuntu 18.10
- Docker(Version: 18.06.1-0ubuntu1). Can be installed with `apt-get install docker.io`
- Docker-Compose(Version: 1.21.0-3). Can be instlled with `apt-get install docker-compose`
- git

Execute the following commands:
    
    cd
    git clone https://gitlab.com/pb.0/sre-challenge.git SOLUTION_PAUL_BEHRISCH
    cd SOLUTION_PAUL_BEHRISCH
    docker-compose up -d

## Running the tests
    
    cd && python3 ~/SOLUTION_PAUL_BEHRISCH/tests/test.py --port 5000 --cert-path ~/SOLUTION_PAUL_BEHRISCH/ssl/app.crt


## Saving a message

    curl --cacert SOLUTION_PAUL_BEHRISCH/ssl/app.crt -X POST -H "Content-Type: application/json" -d '{"message": "test"}' https://localhost:5000/messages


This will output the following:
    
    {"digest":"9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"}    
   
## Retrieving a message

    curl --cacert ~/SOLUTION_PAUL_BEHRISCH/ssl/app.crt https://localhost:5000/messages/9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08

This will return the following:

    {"message":"test"}
 
## Questions


    How would your implementation scale if this were a high throughput service, and how could you improve that?

This would not scale very well in this setup, since it's a single container per service(nginx, app, redis).
Improvements could be made by adding more containers behind a load balancer. I also would not hos
Redis in a container when going to a production environment, since Docker's storage drivers were not that reliable in the past.

https://myopsblog.wordpress.com/2017/02/06/why-databases-is-not-for-containers/
https://thehftguy.com/2016/11/01/docker-in-production-an-history-of-failure/

    How would you deploy this to the cloud, using the provider of your choosing?
    
I'd host the code in a Git repository, use Cloudformation for Infrastructure as a code, and Fargate(ECS) to run containers.

    What would the architecture look like? What tools would you use?

I'd use an AWS load balancer, ECS or EKS for the web and app servers, managed Redis(https://aws.amazon.com/en/redis/), Let's encrypt for the SSL certs,
Elasticsearch/Logstash/Kibana for centralized logging

    How would you monitor this service? What metrics would you collect? How would you act on these metrics?

I'd use health checks with the load balancer to monitor the service.
The important metrics to monitor are requests processed, how long it takes to serve a request
I prefer to connect Grafana to Cloudwatch to display all the data in a proper dashboard.


