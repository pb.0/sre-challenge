#!/usr/bin/env python

from flask import Flask, jsonify, abort, request
from hashlib import sha256
import redis
import json
import os
app = Flask(__name__)
r = redis.Redis(host=os.environ['REDIS_HOST'], port=os.environ['REDIS_PORT'], db=0, decode_responses=True)

@app.route("/messages", methods=["POST"])
def store_message():
    data = request.data
    try:
      dataDict = json.loads(data)
      message = dataDict['message']
    except Exception as e:
      return jsonify(err_msg="Cannot save message"), 500
    hex_message = sha256(message.encode()).hexdigest()
    r.set(hex_message, message)
    return jsonify(digest=hex_message), 201
    
@app.route("/messages/<hash>")
def get_message(hash):
    if r.exists(hash):
      message = str(r.get(hash))
      response = jsonify(message=message)
    else:
      response = jsonify(err_msg="Message not found"),  404
    return response

